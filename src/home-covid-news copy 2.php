
<section class="bit-row bit-covid-news" style="padding-bottom:0; margin-bottom:5rem; margin-top: 3rem">
    <div class="bit-column bit-column-1-2 bit-full-at-800" >
        <h3 style="margin-top:0; font-weight:bold">Protegidos ante el covid-19</h3>

        <p style="font-size:1rem">Algunos tratamientos que se realizan en una clínica dental generan aerosoles y estos pueden quedar en suspensión en el aire hasta 3 h, por eso en Clínica Dental Basi después de cada tratamiento desinfectamos el box con <strong>luz ultravioleta</strong> que deja las superficies y el aire libres de virus para que el paciente sea atendido sin riesgo de contagio.   
        </p>
        
        <a class="button hollow  uk-button-secondary" style="line-height:1.4; font-size:1rem" href="https://clinicadentalbasi.es/2020/12/04/sistema-de-desinfecion-centralizado-del-aire-por-uv-de-purion/">Nuestra Clínica Dental está desinfectada continuamente por UV con el sistema centralizado de aire de Purion --> Más info</a>
        <a class="button hollow icon ion-document-text" style="line-height:1.4; font-size:1rem;border:1px solid #DF5A54; color:#DF5A54;text-align:center" href="https://clinicadentalbasi.es/certificado-de-desplazamiento-2"> Solicitar certificado de desplazamiento</a>

    </div>
  
    <div class="bit-column bit-column-1-2 bit-full-at-800" >
        
        <iframe width="560" height="315" src="https://www.youtube.com/embed/wJB8nyMkTpM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div> 
    
    <h3 style=" margin-bottom:1rem; margin-top: 1rem; font-weight:bold; display:block; width:100%">Así hemos preparado la clínica</h3>

    <div class="bit-column-1-2 bit-full-at-800"  style="margin-bottom:2rem;z-index:-1">
        <?php// get_template_part('components/covid-slider'); ?>
        <?php echo do_shortcode('[slide-anything id="5714"]'); ?>
    </div>
    
   <div class="bit-column bit-column-1-2 bit-full-at-800">

   <iframe width="560" height="315" src="https://www.youtube.com/embed/BMg2MOQKy9g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   
   </div>

</section>


<style>
    .bit-covid-news {
        padding:2rem;
        border: silver solid 1px;
    }

    @media only screen and (max-width: 768px) {
        .bit-covid-news  {
            padding:1rem;
            font-size:1rem;
        }
    }


</style>