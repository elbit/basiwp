<?php get_header(); 
/* Template Name: Blog 2*/
?>

<main role="main" aria-label="Content" class="bit-row">

<div style="list-style:none; font-size: 14px; margin-bottom:2rem;">
<h5>Categorias:</h5>
<?php wp_list_categories(
    array(
        'orderby' => 'name',
        'separator' => ' / ',
        'title_li' => __( '' ),
        'style' => 'span'
)) ?>
</div>

<div class="bit-row" style="align-items:stretch;margin-bottom:0"> 

<?php 
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$latest_blog_posts = new WP_Query( array( 
    'posts_per_page' => 9 ,
    'cat' => '-264,-265,-266,-267,-268',
    'paged' => $paged
    
    ) );

if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post();?>



<article id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-1-3 bit-pill-card bit-full-at-550'); ?> style="display:flex;flex-direction:column;flex-wrap: nowrap; align-items:stretch; justify-content:space-between;">
    <div> <a class="bit-pill-card_thumbnail--big " href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('custom-size-3/4');  ?>
    </a></div>
   
    
    <div class="bit-pill-card-content" style="padding:10px 15px">
        <h4 class="bit-pill-card-title">
            <a  class="bit-u-c-gray" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a> </br>
            <time class="" datetime="<?php the_time('Y-m-d'); ?>" style="font-size:0.8rem;font-weight:normal">
             <i class="ion-clock"></i>  <?php the_date(); ?>
        </time>
        </h4>
        <div class="bit-pill-excerpt" style="margin-bottom:5px">
        <?php the_excerpt(); ?> 
        </div>
    </div>
    
    <div class="bit-pill-card_footer" style="max-height:50px;font-size:0.8rem;font-weight:normal;display:flex;align-items:center">
        <?php _e( '# ', 'html5blank' ); the_category(', '); ?>
        
    </div>
    
</article>

<?php endwhile; ?>

<div style=" height: 100px; font-size:1.4rem; displxay:flex; justify-content:space-around;  margin: 2rem auto; margin-bottom:0; text-align:center">
<?php
$big = 999999999; // need an unlikely integer
 
echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $latest_blog_posts->max_num_pages,
    
    
) );
?>

</div>
<?php else: ?>

   
<article>
    <h3><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h3>
</article>
<?php endif; ?>

</div>
</main>

<?php get_footer(); ?>