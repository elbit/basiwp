<?php /* Template Name: Equipo*/ get_header(); ?>
<main class="bit-row" role="main" aria-label="Content" style="padding: 0 1rem;">
    <h1 class="strong-divider"> <i class="ion-ios-people"></i>
        <?php the_title(); ?>
    </h1>
    <div  class="bit-row" >
        <?php  the_content()?>
    </div>
    
    
    <div class="bit-row" style="margin-top:3rem;display:flex;align-items:stretch; min-height:100%;">
        <?php get_template_part('equipo-loop'); ?>
    </div>
     <?php edit_post_link(); ?>
</main>
<?php get_footer(); ?>
