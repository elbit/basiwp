<?php $latest_blog_posts = new WP_Query( array( 
    'posts_per_page' => 1 ,
    'category_name' => 'articulo-destacado',
    'orderby'       => 'rand'
     ) );

if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post();?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-full bit-pill-card'); ?>>
    <a class="bit-pill-card_thumbnail--big bit-full-at-550 img_fullH" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"  style="overflow:hidden; display:inline-block">
        <?php the_post_thumbnail('custom-size-1/4');  ?>
    </a>
    <div class="bit-pill-card-content bit-full-at-550">
        <h4 class="bit-pill-card-title">
            <a class="bit-u-c-gray bit-u-fsz-2" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a>
        </h4>
        <?php the_excerpt(); ?>
        
    </div>
    <div class="bit-u-bg-primary bit-pill-card_footer ">
            <?php _e( '# ', 'html5blank' ); the_category(', '); ?>
        </div>
    
</article>


<?php endwhile; ?>
<?php else: ?>
<article>
    <h3>
        <?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?>
    </h3>
</article>
<?php endif; ?>
