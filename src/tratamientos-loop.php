<?php // WP_Query arguments
$args = array('category_name' => 'tratamientos');
// The Query
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post(); 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-1-4 item bit-u-mb2 bit-1-2-at-800 bit-full-at-550' ); ?> style="margin-bottom:6rem;display:flex; flex-direction:column; justify-content: space-between;">
    <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('custom-size'); ?>
    </a>
    <h5 class="item_title bit-u-mb1">
        <?php the_title(''); ?>
    </h5>
    <a href="<?php the_permalink(); ?>" class="button hollow secondary expanded">Ir a ficha  <i class="ion-ios-arrow-forward"> </i></a>
</article>
<?php
    }
} else {
}
wp_reset_postdata();?>
