<?php
// $context = Timber::context();
// $args = 'post_type=articulo';
// $context['posts'] = Timber::get_posts();
// $context['xxss'] = array(
//     array (
//     'title' => 'twiter',
//     'url' => 'https://twitter.com',
//     'icon' => 'ion-social-twiter',
//     )
    
// );
?>

<!doctype html>
<html <?php language_attributes();?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="google-site-verification" content="yThuLh1R6pJJ9nTtulQkvl8l9e8qJuqR_a_FHqzbj0Y" />
    <!-- <title>
        <?php// wp_title('');?>    
    </title> --> 
    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name');?>" href="<?php bloginfo('rss2_url');?>" />
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta name="description" content="<?php bloginfo('description');?>"> -->
    
    <?php wp_head();?>
    
    <link rel="stylesheet" rel="preload" href="<?php echo get_template_directory_uri(); ?>/fonts/css/build/ionicons.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;700&display=swap" rel="stylesheet"> 
    

    <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "Horario Clínica",
  "image": "",
  "@id": "clinicadentalbasi.es",
  "url": "clinicadentalbasi.es",
  "telephone": "93 666 91 29",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Carrer del Marquès de Monistrol, 20",
    "addressLocality": "Sant Feliu de Llobregat",
    "postalCode": "08980",
    "addressCountry": "es"
  },
  "sameAs": [
    "https://www.instagram.com/clinicadentalbasi/"
  ],
  "openingHoursSpecification": [
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Friday"
      ],
      "opens": "09:00",
      "closes": "13:00"
    },
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Friday"
      ],
      "opens": "15:00",
      "closes": "19:30"
    },
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Thursday"
      ],
      "opens": "15:00",
      "closes": "19:30"
    }
  ]
}


</script>

<!-- Facebook Pixel Code -->
<!-- <script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '807203680074082');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=807203680074082&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->

<style>
iframe {max-width: 100%;}
* { font-display: swap;}
</style>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrolldepth/1.2.0/jquery.scrolldepth.min.js" defer></script> -->
<script>
// jQuery(function() {
//   jQuery.scrollDepth();
// });
// </script>

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-9SS40WVLRQ"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-9SS40WVLRQ');
</script>

</head>

<body <?php body_class();?> >

    <header id="top-bar" class="bit-row top-bar" role="banner">
        
    
    <div class="top-bar-xxss" style="font-size:0.9rem;">
    
    <a href="https://clinicadentalbasi.es/portal-pacientes/" class="bxutton bitx-column bixt-column-1-4 klincare" onClick="ga('send', 'event', { eventCategory: 'contact hub', eventAction: 'click', eventLabel: 'klincare', eventValue: 1});" style="background-color:#FEF8BE; color:gray ;font-size: 0.8rem; padding:0 5px; height: 25px"><i class="icon ion-person"> </i>Portal pacientes
    <!-- <span class="mobile-copy">Pacientes</span> -->
  </a>    
        <?php Timber::render( 'xxss-icons.twig', $context );?>    
        </div>   
        
        <div class="top-bar-left">
            <a href="<?php echo home_url(); ?>">
                <img class="logo-img" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Logo">
            </a>
        </div>
            
        <nav class="top-bar-right" role="navigation">
            <?php html5blank_nav();?>
        </nav>
    
    </header>


    <div class="bit-wrapper">
       

        <div class="bit-row contact-hub " style="padding:0 0rem; margin-top:3rem;">

        <div class="top-bar-time" style="">
          <div class="open-sans" style="text-align: left;float:right;display:inline-block;font-size:0.9rem;padding:0.5rem 0.5rem;line-height:1; vertical-align:top;margin:0 auto; ">
            <i class="icon ion-clock" style="font-size:1.2rem"> </i> <strong>L-V: </strong>  9:00-13:00 h | 15:00 - 20:00 h 
          </div>
        </div>

            <a href="https://wa.me/34619204389" class="button bit-column bit-column-1-4 whatsapp" onClick="ga('send', 'event', { eventCategory: 'contact hub', eventAction: 'click', eventLabel: 'Whatsapp', eventValue: 1});"  ><i class="icon ion-social-whatsapp" ></i> </i>619 204 389 <span class="mobile-copy">mensaje</span></a>

            <a href="/cita-online-dentista-sant-feliu" class="button bit-column bit-column-1-4 addentra"  title="cita online" onClick="ga('send', 'event', { eventCategory: 'contact hub', eventAction: 'click', eventLabel: 'Cita Online', eventValue: 1});">
                <i class="icon ion-calendar"> </i>
                Cita Online <span class="mobile-copy">Cita</span>
            </a>


            <a href="tel:936669129" class="button bit-column bit-column-1-4 phone " onClick="ga('send', 'event', { eventCategory: 'contact hub', eventAction: 'click', eventLabel: 'Phone', eventValue: 1});"><i class="icon ion-ios-telephone" > </i>93 666 91 29 <span class="mobile-copy">Llamar</span></a>

            <a target="_blank" href="https://www.google.com/maps/place/Clinica+Dental+Basi/@41.381328,2.049448,15z/data=!4m5!3m4!1s0x0:0x2b49771080ecd14c!8m2!3d41.384081!4d2.053074?hl=es" class="button bit-column bit-column-1-4 contacto" onClick="ga('send', 'event', { eventCategory: 'contact hub', eventAction: 'click', eventLabel: 'Mapa', eventValue: 1});"><i class="icon ion-android-pin"> </i>Como llegar <span class="mobile-copy">Ubicación</span></a>

            

        </div>

<?php
    $today = date('Ymd');
    $args  = array(
        'post_type'      => 'avisos',
        'posts_per_page' => 2,
        'meta_query'     => array(
            array(
                'key'     => 'fecha_inicio',
                'compare' => '<=',
                'value'   => $today,
            ),
            array(
                'key'     => 'fecha_final',
                'compare' => '>=',
                'value'   => $today,
            ),
        ),

    );
    $loop = new WP_Query($args);
    
    while ($loop->have_posts()): $loop->the_post();
        the_content();
        echo '<div class="row" data-closable><div class="column callout avis-callout ';
        the_field('tipo_de_aviso');
        echo ' text-center"><i class="ion-ios-information icon"> </i> <small>';
        the_field('avis');
        echo '</small><button class="close-button " aria-label="Dismiss alert " type="button " data-close><span aria-hidden="true ">&times;</span></button></div></div> ';
    endwhile;
?>

<?php wp_reset_query();?>

