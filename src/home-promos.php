

<?php $latest_blog_posts = new WP_Query( array( 'posts_per_page' => 4 ,'category_name' => 'promo') );

if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post();?>


<article id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-1-3 bit-full-at-550 bit-pill-card bit-u-mb3'); ?>  size="<?php the_field('mida'); ?>" >
    <a class="" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('custom-size');  ?>
    </a>
    <?php if( get_field('limite_oferta') ): ?>
        <div class="" style="width:100%; padding: 0.5rem; text-align:center; background-color: LightSalmon "> * Oferta valida hasta el 
        <?php  
            $dateformatstring = "j F";
            $unixtimestamp = strtotime(get_field('limite_oferta'));
            echo date_i18n($dateformatstring, $unixtimestamp); ?>
        </div>
    <?php endif; ?>

    <?php if( get_field('extra_info') ): ?>
        <div class="" style="width:100%; padding: 0.5rem; text-align:center; padding-top:0 ">*
        <?php the_field('extra_info') ?>
        </div>
    <?php endif; ?>
    
    <div class="bit-pill-card-content ">
        <h5 class="bit-u-bold bit-u-t-center">
            <a class="bit-u-c-black bit-u-1-5rem-at-800" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a>
        </h5>
        
    </div>
    
</article>
<?php endwhile; ?>
<?php else: ?>
    <style>
        .promociones {
            display: none; /* Ocultar la sección de promociones */
        }
    </style>
<?php endif; ?>

<style>
.promos {
    padding: 0 5rem;

}

@media print, screen and (max-width: 768px) {
    .promos {
    padding: 0 2rem;

}
}

</style>
