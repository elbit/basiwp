<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	
	<article id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-1-3 bit-pill-card bit-full-at-550'); ?> style="display:flex;flex-direction:column;flex-wrap: nowrap; align-items:stretch; justify-content:space-between;">
		<div> 
		<a class="bit-pill-card_thumbnail--big " href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<?php the_post_thumbnail('custom-size-3/4');  ?>
		</a>
		</div>
   
    
    <div class="bit-pill-card-content" style="padding:10px 15px">
        <h4 class="bit-pill-card-title">
            <a  class="bit-u-c-gray" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
            </a> </br>
            <time class="" datetime="<?php the_time('Y-m-d'); ?>" style="font-size:0.8rem;font-weight:normal">
             <i class="ion-clock"></i>  <?php the_date(); ?>
        </time>
        </h4>
        <div class="bit-pill-excerpt" style="margin-bottom:5px">
        <?php the_excerpt(); ?> 
        </div>
    </div>
    
    <div class="bit-pill-card_footer" style="max-height:50px;font-size:0.8rem;font-weight:normal;display:flex;align-items:center">
        <?php _e( '# ', 'html5blank' ); the_category(', '); ?>
        
    </div>
    
</article>

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
