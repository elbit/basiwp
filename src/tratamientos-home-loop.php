<?php // WP_Query arguments
$args = array(
     'category_name' => 'tratamientos',
      'cat' => '-272'
     
);
// The Query
$query = new WP_Query( $args );
// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post(); ?>

<a href="<?php the_permalink(); ?>"  <?php post_class( 'bit-column bit-column-1-2 bit-pill-card bit-full-at-550'); ?> style="display:flex">
    
        <div class="bit-pill-card_thumbnail" >
            <?php the_post_thumbnail('custom-size-3/4-small'); ?>
        </div>
    
       <div class="bit-pill-card-content">
            <h3 class="bit-pill-card-title bit-u-va-middle" >
                <?php the_title(''); ?>
            </h3>
            <i class="bit-pill-card-button ion-ios-arrow-right bit-u-va-middle-r"></i>
        </div>
</a>

<?php

    }
} else {
   
}
wp_reset_postdata();;?>
