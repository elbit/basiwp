
<?php 
$args = array(
    'category_name' => 'equipo' ,
    'order' => 'asc'
);
// the query
$the_query = new WP_Query( $args );
?>


<?php if ( $the_query->have_posts() ) :?>

	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		
<div id="post-<?php the_ID(); ?>" <?php post_class( 'bit-column bit-column-1-4 bit-1-2-at-800 bit-full-at-550'); ?> style="margin-bottom:6rem;display:flex; flex-direction:column; justify-content: space-between;">
    
    <a class="thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail(''); ?>
    </a>
    <h5 class="item_title">
        <?php the_title(''); ?>
        
        <?php if( get_field('linkedin') ): ?>
        <a href="<?php the_field('linkedin'); ?>" target="_blank"> <i class="ion-social-linkedin"></i></a>
        <?php endif; ?>
    
    </h5>
    
    <h6 class="subheader item_sub-title"><i class="ion-person"> </i> <?php the_field('cargo');?></h6>
    
    <a href="<?php the_permalink(); ?>" class="button hollow secondary expanded" style="font-size:1rem; align-self:flex-end; font-weight:bold;  ">Ir a ficha  <i class="ion-ios-arrow-forward"> </i></a>
</div>
<?php endwhile; ?>

<?php wp_reset_postdata(); ?>

<?php else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


