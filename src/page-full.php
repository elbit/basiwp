<?php /* Template Name: Full Page*/ get_header(); ?>
<main class="bit-row" role="main" aria-label="Content">
    <h1 class="strong-divider"> 
        <?php the_title(); ?>
    </h1>
    <div class="bit-column">
    <?php  the_content()?>
    </div>
  
     <?php edit_post_link(); ?>
</main>
<?php get_footer(); ?>
