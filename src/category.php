<?php get_header(); ?>

	<main role="main" aria-label="Content" class="bit-row">
		
		<h1><?php _e( 'Categoria: ', 'html5blank' ); single_cat_title(); ?></h1>
		<div class="bit-row" style="align-items:stretch;margin-bottom:0"> 

			<?php get_template_part('loop'); ?>

			

		</div>
		
	</main>

<?php get_footer(); ?>
