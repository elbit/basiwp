
<!-- Slider main container -->
<div class="swiper-container" style="max-width:800px; z-index:-1" >
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper " >
        <!-- Slides -->
       
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/5.jpg" alt=""></div>
        
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/2.jpg" alt=""></div>
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/3.jpg" alt=""></div>
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/1.jpg" alt=""></div>
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/7.jpg" alt=""></div>
        <div class="swiper-slide"><img class="x" src="<?php echo get_template_directory_uri(); ?>/img/covid/6.jpg" alt=""></div>
      
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button swiper-button-prev"></div>
    <div class="swiper-button swiper-button-next"></div>

    <!-- If we need scrollbar -->
    <div class="swiper-scrollbar"></div>
</div>

<script>
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    centeredSlides: true,
    direction: 'horizontal',
    loop: true,
    slidesPerView: 'auto',
  
  

    // If we need pagination
   
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  })
  </script>

  <style>
  .x {
     max-height: 100%;
}

/* .swiper-slide {
  width:auto!important;
} */


.swiper-pagination,.swiper-button {
      color:white;
      font-size:3rem
      
    }

  
  </style>
