<section class="bit-row  social-hub">
        <h3 class="strong-divider"><i class="ion-social-rss"> </i>Redes Sociales</h3>
        
        <ul class="bit-row bit-column-full">
            <li>
                <a target="_blank" href="https://www.facebook.com/dentalbasi"><i class="ion-social-facebook icon-large bit-u-c-facebook" > </i></a>
            </li>
            <li>
                <a target="_blank" href="https://twitter.com/DentalBasi"><i class="ion-social-twitter icon-large bit-u-c-twitter"> </i></a>
            </li>
            <li>
                <a target="_blank" href="https://www.youtube.com/c/ClinicadentalbasiEs"><i class="ion-social-youtube icon-large bit-u-c-youtube"> </i></a>
            </li>
            <li>
                <a href="https://www.instagram.com/clinicadentalbasi" alt="instagram"><img src="<?php echo get_template_directory_uri(); ?>/img/rss/instagram.svg" alt="" target="_blank"></a>
            </li>
            <li> <a target="_blank" href="https://masquemedicos.com/dentista_sant-feliu-de-llobregat/clinica-dental-basi/" style="max-width:200px;"><img src="https://s2.masquemedicos.org/images/logo-medicos.png" alt="" ></a></li>
            <li>
                <a target="_blank" href="http://www.doctoralia.es/centro-medico/clinica+dental+basi-1078553" rel="publisher" target="_top" style="text-decoration:none;" class="dralia-social-widget">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/rss/logo-doctoralia.png" alt="Clínica Dental Basi en Doctoralia" title="Clínica Dental Basi en
Doctoralia" >
                </a>
            </li>
                 
        </ul>
    </section>