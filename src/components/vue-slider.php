
<!-- Slider main container -->
<div class="swiper-container bit-row" style="heigth:auto">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <div class="swiper-slide">Slide 1 Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sed fugiat, cupiditate placeat reiciendis ab sint incidunt autem labore quidem libero tempora a itaque magnam magni cumque. Odio, voluptate minus repudiandae quaerat rem sit numquam ipsum fugit est autem, eveniet excepturi. Maiores et repellendus, vero hic saepe nemo, dolorem mollitia temporibus obcaecati perspiciatis necessitatibus consequuntur consectetur vitae fugiat error id ratione quo impedit fugit ullam eaque suscipit omnis neque libero? Id nobis perspiciatis, excepturi similique, laudantium accusamus doloribus aliquam distinctio voluptatibus accusantium ea, illo porro et minus aspernatur quas voluptas? Ut dolores dolor mollitia omnis expedita excepturi sunt voluptas natus consequuntur!</div>
        <div class="swiper-slide">Slide 2</div>
        <div class="swiper-slide">Slide 3</div>
        ...
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>

    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

    <!-- If we need scrollbar -->
    <div class="swiper-scrollbar"></div>
</div>

<script>
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    // And if we need scrollbar
    scrollbar: {
      el: '.swiper-scrollbar',
    },
  })
  </script>
