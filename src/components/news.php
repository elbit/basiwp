<ul class="">
<?php $latest_blog_posts = new WP_Query( array( 'posts_per_page' => 4 ,'cat' => '-264,-265,-266,-267,-268') );
if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post();?>
<ul>


<li><a href="" style="font-size: 1rem;"><?php the_title(); ?></a></li>


<?php endwhile; ?>
<?php else: ?>
<article>
    <h3>
        <?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?>
    </h3>
</article>
<?php endif; ?>

</ul>