<section class="bit-row bit-u-flex-acenter bit-u-flex-jc-start proveidors " >
        <h3 class="strong-divider">Proveedores</h3>
        
            <a target="_blank" href="https://addentra.com/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/addentra.png">
            </a>
            <a target="_blank" href="https://invisalign.es/es/Pages/Home.aspx">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/logo-invisaling.jpg">
            </a>
            <a target="_blank" href="https://www.suresmile.com/es-ib/" >
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/logo-suresmile.png" alt="suresmile">
            </a>
            
             <a target="_blank" href="https://www.kavo.com/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/kavo.png">
            </a>
            <a target="_blank" href="http://misiberica.es">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/mis-iberica.png">
            </a>

             <!-- <a target="_blank" href="https://www.mondental.com/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/mondental.png">
            </a> -->
            <a target="_blank" href="http://inibsa.com/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/inibsa.png">
            </a>
            <a target="_blank" href="http://www.ortoplus.es/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/ortoplus.png">
            </a> 
            <a target="_blank" href="http://coec.cat">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/coec.png"> </a>
          
            <a target="_blank" href="http://dosimetria.com/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/dosimetria.jpg">
            </a>
            <a target="_blank" href="https://bancsabadell.com">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/banc-sabadell.png">
            </a>
            <a target="_blank" href="http://elegegrafiques.com">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/elege.png">
            </a>
            <a target="_blank" href="http://es.acteongroup.com/index.php">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/acteon.png">
            </a>
            <a target="_blank" href="http://www.jmpelegri.es/">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/pelegri.png">
            </a>
            <a target="_blank" href="https://radmedica.es">
                <img src="<?php echo get_template_directory_uri(); ?>/img/proveidors/radmedica.png">
            </a>
       
    </section>

    