<?php get_header();

$args = array(
    'post_type' => 'post',
    'category_name' => 'promo',
    'orderby'        => 'rand',
    'posts_per_page' => 3,
);

$context['promo_posts'] = Timber::get_posts($args);
?>

<main role="main" aria-label="Content" class="bit-row page-single">
    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="strong-divider"  style="margin-bottom:2rem">
            
            <h1 style="margin-bottom:5px; font-size:1.2rem"><?php the_title();?></h1>
            
            <?php if(!in_category( 'promo' )) : ?>
            <time datetime="<?php the_time('Y-m-d'); ?>" >
                <i class="ion-ios-clock-outline"> </i> <?php the_date(); ?>
            </time>
            <?php endif; ?>
            
            <?php _e( '# ', 'html5blank' ); the_category(', '); ?>
            
        </header>
        <?php the_post_thumbnail(); ?>
        
        <div class=""><?php the_content();?></div>
        
        <?php edit_post_link();  ?>
      
    </article>
    
    <?php endwhile; ?>
    
    <?php else: ?>
    <article>
        <h1>
            <?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?>
        </h1>
    </article>
    
    <?php endif; ?>
</main>

<?php Timber::render( 'related_posts.twig', $context );?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
