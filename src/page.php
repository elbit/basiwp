<?php get_header();?>
<main role="main" aria-label="Content" class="bit-row page-single">

   <h1 class="strong-divider">
            <?php the_title();?>
    </h1>

<?php if (have_posts()): while (have_posts()): the_post();?>

            <article id="post-<?php the_ID();?>" <?php post_class('bit-row');?>>
                <?php the_content();?>

                <?php edit_post_link();?>
            </article>

            <?php endwhile;?>
        <?php else: ?>

    <article>
        <h3>
            <?php _e('Sorry, nothing to display.', 'html5blank');?>
        </h3>
    </article>
    <?php endif;?>

</main>

<?php get_footer();?>
