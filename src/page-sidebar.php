<?php  /* Template Name: Page sidebar */ get_header(); ?>

	<main role="main" aria-label="Content" class="bit-row">
		<!-- section -->
		<section class="bit-column bit-column-1-3 ">

			<h1><?php the_title(); ?></h1>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h3><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h3>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section><!-- /section -->
	
		<aside class="bit-column bit-column-2-3">
		    <?php get_sidebar(); ?>
		</aside>

	</main>

<?php get_footer(); ?>
