<!-- footer -->
<footer class="bit-row site-footer" role="contentinfo">

    <div class="bit-column">
        <p class="copyright">
            &copy;
            <?php echo date('Y'); ?>
            <?php bloginfo('name');?>. /
            <i class="ion-ios-navigate"> </i> Carrer del Marquès de Monistrol, 20, Sant Feliu de Llobregat, Barcelona / <i class="ion-email"> </i>
            <a href="mailto:info@clinicadentalbasi.es">info@clinicadentalbasi.es</a> / <i class="ion-ios-telephone"></i> 93 666 91 29 /
            <a href="/politica-de-privacidad">Política de Privacidad</a>
        </p>
        <p class="">Clínica dental autoritzada pel Registre de Centres, Serveis i Establiments Sanitaris del Departament de Salut de la Generalitat de Catalunya. <strong>Nº E 08700010</strong> </pl>
    </div>
     <!-- <div class="bit-column">
        <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-petit.png" alt="" style="max-width:60px;"></a>
    </div> -->
</footer>
<!-- /footer -->
</div><!-- /wrapper -->

<?php wp_footer();?>

<?php if ( is_page('Equipo') ): ?>

<script src="https://cdn.jsdelivr.net/gh/toddmotto/fluidvids@2.4.1/dist/fluidvids.min.js"></script>
<script>
fluidvids.init({
  selector: ['iframe', 'object'], // runs querySelectorAll()
  players: ['www.youtube.com', 'player.vimeo.com'] // players to support
});</script> 

<?php endif; ?>


<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/lib/jquery.sticky.js"></script>

<script> jQuery(document).foundation();</script>

<script>
window.addEventListener('scroll', function () {

    if (window.pageYOffset < 500 && window.pageYOffset > 60) { 
        jQuery('.top-bar,.bit-menu,.contact-hub').css('opacity', '0.2');

    }
    else if (window.pageYOffset >= 500) {

        
        
        jQuery(".top-bar").sticky({
                    topSpacing: 0,
                    zIndex: 1,
                    wrapperClassName: 'sticky-wrapper-top-bar',
                    className:'top-bar-is-sticky',
                    getWidthFrom: 'body'
                });

                jQuery(".contact-hub").sticky({
                    topSpacing: 0,
                    wrapperClassName: 'sticky-wrapper-contact-hub',
                    className:'contact-hub-is-sticky',
                    getWidthFrom: 'body',
                    zIndex: 1,
                });
        jQuery('.top-bar,.bit-menu,.contact-hub').css('opacity', '1');
        //jQuery('body').css('padding-top','160px');
  } else {
    jQuery(".top-bar, .contact-hub").unstick();
    jQuery('.top-bar,.bit-menu,.contact-hub').css('opacity', '1');
    // jQuery('body').css('padding-top','0');
    }
});
</script>

<script>
(function(f, i, r, e, s, h, l) {
    i['GoogleAnalyticsObject'] = s;
    f[s] = f[s] || function() {
        (f[s].q = f[s].q || []).push(arguments)
    }, f[s].l = 1 * new Date();
    h = i.createElement(r),
        l = i.getElementsByTagName(r)[0];
    h.async = 1;
    h.src = e;
    l.parentNode.insertBefore(h, l)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-25447234-1', 'clinicadentalbasi.es');
ga('send', 'pageview');
</script>

</body>
</html>
