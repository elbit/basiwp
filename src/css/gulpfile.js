const gulp = require('gulp')
const purgecss = require('gulp-purgecss')
const purgecssWordpress = require('purgecss-with-wordpress')

gulp.task('purgecss', () => {
    return gulp
        .src('foundation/css/app.css')
        .pipe(purgecss({
            content: ['../**/*.php','../**/*.twig','../**/*.html'],
            safelist: purgecssWordpress.safelist
        }))
        .pipe(gulp.dest('build'))
})

gulp.task('purgecss-ion', () => {
    return gulp
        .src('../fonts/css/ionicons.min.css')
        .pipe(purgecss({
            content: ['../**/*.php','../**/*.twig','../**/*.html'],
            safelist: purgecssWordpress.safelist
        }))
        .pipe(gulp.dest('../fonts/css/build'))
})
