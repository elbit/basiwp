<?php /* Template Name: Home */get_header(); ?>
<main id="app" role="main" aria-label="Content" style="" class="main">

<?php //get_template_part('components/vue-slider'); ?>


<?php // get_template_part('home-covid-news'); ?>
   
   <section class="bit-row intro" style="margin-bottom: 0;">
   <h1 class="strong-divider" style="font-size:1.8rem!important;"><i class="ion-ios-home"> </i> Clínica dental Basi: Tu dentista de confianza en Sant Feliu de Llobregat</h1>
        <?php get_template_part('home-custom-content'); ?>
      
    </section>
    <section class="bit-row promociones" id="promociones"  style="margin-bottom:0">
        <h3 class="strong-divider"><i class="ion-pricetag"> </i> Promociones </h3>
        <div class="bit-row promos" >
            <?php get_template_part('home-promos'); ?>
        </div>
    </section>
    
    <section class="bit-row intro" style="margin-bottom: 0;">
    <h3 class="strong-divider"><i class="ion-social-rss"> </i> Redes sociales<h3>
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/instagram.png"" alt=""></br>
        <?php get_template_part('components/insta-feed'); ?>

        <h3 class="strong-divider"><i class="ion-person"> </i> Reseñas de nuestros clientes<h3>
        
        
   
        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/g-bussines.png"" alt="">
        <?php get_template_part('components/google-ratings'); ?>

    </section>
    

    <section class="bit-row" style="margin-bottom:0">
        <?php get_template_part('home-extra'); ?>
    </section>

    <section class="bit-row home-news" style="margin-bottom:0">
        <h3 class="strong-divider"><i class="ion-ios-book"> </i> Noticias de tu dentista</h3>
        <main class="bit-column bit-full-at-800">
            
             <?php// get_template_part('home-featured-news'); ?>
            
            <div class="bit-row" style="align-items:stretch;margin-bottom:0"> 
                <?php get_template_part('home-news'); ?>
                <a class="button hollow expanded" href="/blog" style="font-weight:bold; font-size:1.5rem"> Más Noticias</a>
            </div>
        
        </main>
    
    </section>
    
   
    <?php get_template_part('components/proveidors'); ?>
   
</main>
<?php get_footer(); ?>
